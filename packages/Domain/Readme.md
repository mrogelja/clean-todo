# Domain

The domain (short for ‘business domain’; also known as core) is the reason why your app exists.

It’s the added value to the business/users/world; it contains your app’s identity (such as a eukaryotic cell that houses DNA in its nucleus).

It’s supposed to be independent of specific technologies like databases or web APIs and it shouldn't be tied to frameworks.

All it contains is business logic and your domain modeling.

Domain code uses the ubiquitous language, i.e., the agreed-upon language (DDD).
