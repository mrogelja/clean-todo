import { Todo } from "../../Entity/Todo/Todo";

export interface TodoRepository {
    getTodos(): Promise<Todo[]>

    getTodo(id: string): Promise<Todo>

    createTodo(todo: Omit<Todo, "id">): Promise<Todo>

    updateTodo(todo: Todo): Promise<Todo>

    removeTodo(id: string): Promise<boolean>
}

