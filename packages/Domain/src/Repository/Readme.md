# Repository folder

This is a place for all repository interfaces (or ports).

The repository is a central place to keep all model-specific operations.

For example, the Todo repository interface would describe repository methods.

The actual repository implementation will be kept in the Infrastructure layer.
