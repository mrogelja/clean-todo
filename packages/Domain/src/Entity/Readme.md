# Entities

This is the place for all models interfaces.
A model typically represents a real-world object that is related to the domain problem.
