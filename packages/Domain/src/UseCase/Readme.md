# UseCases

Use cases (also known as ‘application services’) are pieces of functionality that map user stories into code; they're the reasons to build and use the app.

The use cases are the business rules for a specific application. They tell how to automate the system. This determines the behavior of the app.

Here is an example from the book of the business rules for a use case (p. 192, somewhat modified):

```fixtures
  Gather Info for New Loan

  Input:  Name, Address, Birthdate, etc.
  Output: Same info + credit score

  Rules:
    1. Validate name
    2. Validate address, etc.
    3. Get credit score
    4. If credit score < 500 activate Denial
    5. Else create Customer (entity) and activate Loan Estimation
```

The use cases interact with and depend on the entities, but they know nothing about the layers further out.
They don’t care if it’s a web page or an iPhone app. They don’t care if the data is stored in the cloud or in a local SQLite database.

This layer defines interfaces or has abstract classes that outer layers can use.
