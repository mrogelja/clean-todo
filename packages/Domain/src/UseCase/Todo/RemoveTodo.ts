import { Todo } from "@domain/Entity/Todo/Todo";
import { TodoRepository } from "@domain/Repository/Todo/TodoRepository";
import { UseCase } from "../UseCase";

/**
 * DTO
 */
export namespace RemoveTodo{
    export type Request  = string
    export type Response = boolean
}

/**
 * UseCase
 */
export class RemoveTodo implements UseCase<RemoveTodo.Request, RemoveTodo.Response>{
  constructor(private todoRepository: TodoRepository) {
  }

  execute(id: RemoveTodo.Request) : Promise<RemoveTodo.Response>{
    return this.todoRepository.removeTodo(id);
  }
}
