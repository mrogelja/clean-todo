import { Todo } from "@domain/Entity/Todo/Todo";
import { TodoRepository } from "@domain/Repository/Todo/TodoRepository";
import { UseCase } from "../UseCase";

export namespace CreateTodo{
    export type Request  = Omit<Todo, "id">
    export type Response = Todo
}

export class CreateTodo implements UseCase<CreateTodo.Request, CreateTodo.Response>{
  constructor(private todoRepository: TodoRepository) {
  }

  async execute(todoData: CreateTodo.Request): Promise<CreateTodo.Response> {
    return this.todoRepository.createTodo({
      ...todoData,
      checked: false
    });
  }
}

