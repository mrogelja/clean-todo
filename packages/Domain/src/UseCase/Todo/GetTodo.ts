import { Todo } from "@domain/Entity/Todo/Todo";
import { TodoRepository } from "@domain/Repository/Todo/TodoRepository";
import { UseCase } from "../UseCase";


export namespace GetTodo{
    export type Request  = string
    export type Response = Todo
}

export class GetTodo implements UseCase<GetTodo.Request, GetTodo.Response>{
  constructor(private todoRepository: TodoRepository) {
  }

  execute(id: GetTodo.Request): Promise<GetTodo.Response>{
    return this.todoRepository.getTodo(id);
  }
}
