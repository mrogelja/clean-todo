import { Todo } from "@domain/Entity/Todo/Todo";
import { TodoRepository } from "@domain/Repository/Todo/TodoRepository";
import { UseCase } from "../UseCase";

export namespace GetTodos{
    export type Request  = string
    export type Response = Todo[]
}

export class GetTodos implements UseCase<GetTodos.Request, GetTodos.Response>{
  constructor(private todoRepository: TodoRepository) {
  }

  execute(): Promise<GetTodos.Response> {
    return this.todoRepository.getTodos();
  }
}
