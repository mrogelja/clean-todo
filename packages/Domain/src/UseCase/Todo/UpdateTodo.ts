import { Todo } from "@domain/Entity/Todo/Todo";
import { TodoRepository } from "@domain/Repository/Todo/TodoRepository";
import { UseCase } from "../UseCase";

/**
 * DTO
 */
export namespace UpdateTodo{
    export type Request  = Todo
    export type Response = Todo
}

/**
 * UseCase
 */
export class UpdateTodo implements UseCase<UpdateTodo.Request, UpdateTodo.Response>{
  constructor(private todoRepository: TodoRepository) {
  }

  execute(todo: UpdateTodo.Request): Promise<UpdateTodo.Response> {
    return this.todoRepository.updateTodo(todo);
  }
}

